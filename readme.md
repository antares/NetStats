# NetStats

------------------

Source code for fetching per process network details <sup>[1](#details)</sup>


----

>**Project File Details**

>**ccode** : This dir has C version of the code <sup>[2](#tcp)</sup>

>**cpp** : This dir has the C++ version of the code

>**Output.txt** :This file has sample output given by the code  

> **Note:**

> - Due to this project being under development, the code is currently printing a lot of debug statements and other details which eventually are used to calculate per process network stats so this [Output.txt](https://gitlab.gnome.org/antares/NetStats/blob/master/Output.txt) file has been included (for clear reference to the result among other details)

Using the code 
---------------------
To compile
```
make 
```
To run 
```shell 
sudo ./a.out
```
-----
<a href="details">[1]Details have been mentioned in the proposal</a>

<a href="tcp">[2]Currently network stats are being collected for TCP ,in the final version UDP will alsobe covered</a>
